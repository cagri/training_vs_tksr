package tk.sr.exerciseservice.service;
import org.springframework.stereotype.Service;
import tk.sr.exerciseservice.entity.Exercise;
import java.util.List;
import java.util.Optional;

@Service
public interface ExerciseService {
    Iterable<Exercise> getExercises();
    Optional<Exercise> getExerciseById(Integer ExerciseId);
    Exercise addExercise(Exercise exercise);
}
