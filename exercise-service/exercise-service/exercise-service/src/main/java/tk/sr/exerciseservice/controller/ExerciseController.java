package tk.sr.exerciseservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tk.sr.exerciseservice.entity.Exercise;
import tk.sr.exerciseservice.repository.ExerciseRepository;
import tk.sr.exerciseservice.service.ExerciseService;

@RestController
public class ExerciseController {

    private final ExerciseService exerciseService;
    private final ExerciseRepository exerciseRepository;

    public ExerciseController(ExerciseService exerciseService, ExerciseRepository exerciseRepository) {
        this.exerciseService = exerciseService;
        this.exerciseRepository = exerciseRepository;
    }

    @GetMapping("/exercises")
    public Iterable<Exercise> getExercises(){
        return exerciseService.getExercises();
    }

    @PostMapping("/exercises")
    public Exercise addExercises(@RequestBody Exercise exercise){
        return exerciseService.addExercise(exercise);
    }
}

