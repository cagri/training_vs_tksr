package tk.sr.exerciseservice.service;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import tk.sr.exerciseservice.entity.Exercise;
import tk.sr.exerciseservice.repository.ExerciseRepository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Service
public class PersistentExerciseService implements ExerciseService{
    private final ExerciseRepository exerciseRepository;
    public PersistentExerciseService(ExerciseRepository exerciseRepository ){
        this.exerciseRepository = exerciseRepository;
    }


    @PostConstruct
    public void init(){
        exerciseRepository.save(new Exercise(1,"Glute Kicks", 10, 3, 18.0));
        exerciseRepository.save(new Exercise(2,"Hip Thrusts", 10, 3, 30.2));
        exerciseRepository.save(new Exercise(3,"Face Pulls", 10, 3, 28.0));
    }

    @Override
    public Iterable<Exercise> getExercises() {
        return exerciseRepository.findAll();
    }

    @Override
    public Optional<Exercise> getExerciseById(@PathVariable "exerciseId" Integer exerciseId) {
        return exerciseRepository.findById(exerciseId);
    }


    @Override
    public Exercise addExercise(Exercise exercise) {
        return exerciseRepository.save(exercise);
    }



}

