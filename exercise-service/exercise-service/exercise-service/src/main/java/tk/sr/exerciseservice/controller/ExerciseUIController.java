package tk.sr.exerciseservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import tk.sr.exerciseservice.entity.Exercise;
import tk.sr.exerciseservice.service.ExerciseService;

@Slf4j
@Controller
public class ExerciseUIController {
    private final ExerciseService exerciseService;

    public ExerciseUIController(ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }


    @GetMapping("/web/exercises")
    public String exercises(Model model){//Model ist Verbindung zwischen Controller und dem Frontend
        var exercises = exerciseService.getExercises();
        model.addAttribute("exercise", new Exercise());
        model.addAttribute("exercises", exercises);
        return "index";
    }

    @GetMapping("/web/exercises/1")
    public String exercise(Model model){//Model ist Verbindung zwischen Controller und dem Frontend
        var exercises = exerciseService.getExerciseById(1);
        model.addAttribute("exercise", new Exercise());
        model.addAttribute("exercises", exercises);
        return "index";
    }

    @PostMapping("/web/exercises")
    public String saveExercise(@ModelAttribute Exercise exercise){
        log.info("Received POST from Thymeleaf with exercise {} ", exercise);
        exerciseService.addExercise(exercise);
        return "redirect:/web/exercises";
    }
}
