package tk.sr.trainingservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tk.sr.trainingservice.entity.Exercise;

import java.util.List;

@FeignClient(name ="ExerciseService", url = "http://localhost:8070")
public interface ExerciseService{
    @RequestMapping(method = RequestMethod.GET, value = "/exercises")
    List<Exercise> getExercises();

    @RequestMapping(method = RequestMethod.GET, value = "/exercises/{id}")
    Exercise getExerciseById(@PathVariable Integer id);

}
