package tk.sr.trainingservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tk.sr.trainingservice.entity.Athlete;


import java.util.List;

@FeignClient(name ="AthleteService", url = "http://localhost:8080")
public interface AthleteService {
    @RequestMapping(method = RequestMethod.GET, value = "/athletes")
    List<Athlete> getAthletes();

    @RequestMapping(method = RequestMethod.GET, value = "/athletes/{id}")
    Athlete getAthleteById(@PathVariable Integer id);


}
