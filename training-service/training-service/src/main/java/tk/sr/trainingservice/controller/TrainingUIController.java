package tk.sr.trainingservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import tk.sr.trainingservice.entity.Athlete;
import tk.sr.trainingservice.entity.Exercise;
import tk.sr.trainingservice.service.AthleteService;
import tk.sr.trainingservice.service.ExerciseService;

import java.util.List;


@Slf4j
@Controller
public class TrainingUIController {
    private final AthleteService athleteService;
    private final ExerciseService exerciseService;


    public TrainingUIController(AthleteService athleteService, ExerciseService exerciseService) {
        this.athleteService = athleteService;
        this.exerciseService = exerciseService;
    }


    @GetMapping("/web/trainings")
    public String trainings(Model model) {//Model ist Verbindung zwischen Controller und dem Frontend
        var athletes = athleteService.getAthletes();
        model.addAttribute("athlete", new Athlete());
        model.addAttribute("athletes", athletes);
        var exercises = exerciseService.getExercises();
        model.addAttribute("exercise", new Exercise());
        model.addAttribute("exercises", exercises);
        return "index";
    }

    @PostMapping("/web/trainings")
    public String saveTraining(@ModelAttribute Athlete athlete) {
        log.info("Received POST from Thymeleaf with athletes {} ", athlete);
        return "redirect:/web/trainings";
    }

    @GetMapping("/web/trainingsZwei")
    public String trainingsZwei(Model model) {//Model ist Verbindung zwischen Controller und dem Frontend
        var athletes = athleteService.getAthleteById(1);
        model.addAttribute("athlete", new Athlete());
        model.addAttribute("athletes", athletes);
        var exercises = exerciseService.getExercises();
        model.addAttribute("exercise", new Exercise());
        model.addAttribute("exercises", exercises);
        return "index";
    }

}
