package tk.sr.trainingservice.entity;

import lombok.Data;

@Data
public class Athlete {
    private int athleteId;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private int age;
    private double height;
    private double weight;
}
