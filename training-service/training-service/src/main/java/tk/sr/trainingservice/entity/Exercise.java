package tk.sr.trainingservice.entity;

import lombok.Data;


@Data

public class Exercise {
    private int exerciseId;
    private String name;
    private int reps;
    private int sets;
    private double weight;
}
