package tk.sr.trainingservice.entity;

import lombok.Data;

import java.util.List;

@Data
public class Training {
    private Athlete athlete;
    private List<Exercise> exercises;
}


