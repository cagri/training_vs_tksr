package tk.sr.athleteservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tk.sr.athleteservice.entity.Athlete;


@Repository
public interface AthleteRepository extends CrudRepository<Athlete,Integer> {

}
