package tk.sr.athleteservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import tk.sr.athleteservice.entity.Athlete;
import tk.sr.athleteservice.service.AthleteService;
import tk.sr.athleteservice.service.PersistentAthleteService;

import java.util.Optional;

@Slf4j
@Controller
public class AthleteUIController {
    /*private final AthleteService athleteService;*/
    private final PersistentAthleteService athleteService;

    public AthleteUIController(PersistentAthleteService athleteService) {
        this.athleteService = athleteService;
    }


    @GetMapping("/web/athletes")
    public String athletes(Model model){//Model ist Verbindung zwischen Controller und dem Frontend
        var athletes = athleteService.getAthletes();
        model.addAttribute("athlete", new Athlete());
        model.addAttribute("athletes", athletes);
        return "index";
    }
    @PostMapping("/web/athletes")
    public String saveAthlete(@ModelAttribute Athlete athlete){
        log.info("Received POST from Thymeleaf with athlete {} ", athlete);
        athleteService.addAthlete(athlete);
        return "redirect:/web/athletes";
    }

    @GetMapping("/web/athletesZwei")
    public String athletesZwei(Model model){//Model ist Verbindung zwischen Controller und dem Frontend
        var athlete = athleteService.getAthleteById(1);
        log.info(athlete.toString());
        model.addAttribute("athlete", new Athlete());
        model.addAttribute("athletes", athlete);
        return "index";
    }

    @GetMapping("/web/athletes/{id}")
    public Optional<Athlete> getTutorialById(@PathVariable Integer athleteId) {
        Optional<Athlete> athlete = athleteService.findById(athleteId);
        log.info(athlete.toString());
        return athlete;
    }

}
