package tk.sr.athleteservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Athlete {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int athleteId;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private int age;
    private double height;
    private double weight;
}