package tk.sr.athleteservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tk.sr.athleteservice.entity.Athlete;
import tk.sr.athleteservice.repository.AthleteRepository;
import tk.sr.athleteservice.service.AthleteService;

import java.util.List;

@RestController
public class AthleteController {
    private final AthleteService athleteService;
    private final AthleteRepository athleteRepository;


    public AthleteController(AthleteService athleteService, AthleteRepository athleteRepository){
        this.athleteService = athleteService;
        this.athleteRepository = athleteRepository;
    }

    @GetMapping("/athletes")
    public Iterable<Athlete> getAthletes(){

        return athleteService.getAthletes();
    }

    @PostMapping(value = "/athletes")
    public Athlete addAthlete(@RequestBody Athlete athlete){
        return athleteService.addAthlete(athlete);
    }

}
