package tk.sr.athleteservice.service;

import org.springframework.stereotype.Service;
import tk.sr.athleteservice.entity.Athlete;

import java.util.List;
import java.util.Optional;

@Service
public interface AthleteService {
    Iterable<Athlete> getAthletes();
    Athlete addAthlete(Athlete athlete);
    Optional<Athlete> getAthleteById(Integer Id);

    Optional<Athlete> findById(long athleteId);
}
