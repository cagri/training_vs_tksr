package tk.sr.athleteservice.service;

import org.springframework.stereotype.Service;
import tk.sr.athleteservice.entity.Athlete;
import tk.sr.athleteservice.repository.AthleteRepository;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class PersistentAthleteService implements AthleteService{
    private final AthleteRepository athleteRepository;

    public PersistentAthleteService(AthleteRepository athleteRepository){
        this.athleteRepository = athleteRepository;
    }

    @PostConstruct
    public void init(){
        athleteRepository.save(new Athlete(1,"theresakl", "blabla", "Theresa", "Klopfer",20, 190, 90.0));
        athleteRepository.save(new Athlete(2,"sophiari", "bliblu", "Sophia", "Rieger",21, 180, 80.0));
        athleteRepository.save(new Athlete(3,"maxi", "musteri", "Max", "Muster",30, 180, 100.0));
    }

    @Override
    public Iterable<Athlete> getAthletes() {
        return athleteRepository.findAll();
    }


    public Optional<Athlete> getAthleteById(Integer Id){
        return athleteRepository.findById(Id);
    }

    @Override
    public Optional<Athlete> findById(long athleteId) {
        return Optional.empty();
    }

    @Override
    public Athlete addAthlete(Athlete athlete) {
        return athleteRepository.save(athlete);
    }

}
